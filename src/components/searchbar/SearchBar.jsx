import React, { useCallback } from "react";
import { Box, TextField } from "@material-ui/core";
import Search from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close"

export function SearchBar({ searchTerm, updateSearchTerm }) {
  const updateFilter = useCallback(value => updateSearchTerm(value), [searchTerm]);
  return (
    <Box>
      <TextField
        onChange={({target: {value}}) => updateFilter(value)}
        value={searchTerm || ""}
        variant="filled"
        InputProps={{
          startAdornment: (
           <Search/>
          ),
          endAdornment: (
            <CloseIcon onClick={()=> updateFilter(null)}/>
          )
        }}
        fullWidth
      />
    </Box>
  )
}
