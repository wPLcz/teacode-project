import { Box, useTheme, Typography } from "@material-ui/core";

export function Header() {
  const theme = useTheme();
  return (
    <Box
      style={{ background: `linear-gradient(90deg, hsla(180, 59%, 58%, 1) 0%, hsla(135, 32%, 70%, 1) 100%)` }}
    >
      <Box display="flex" justifyContent="center" style={{ color: theme.palette.common.white }}>
        <Typography variant="h5" color="inherit">
          Contacts
        </Typography>
      </Box>
    </Box>
  )
}
