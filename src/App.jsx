import { useEffect, useState } from "react";
import axios from "axios";
import { Backdrop, Box, Checkbox, Grid, Avatar, CircularProgress, TableContainer, Table, TableBody, TableRow, TableCell, Typography } from "@material-ui/core";
import { Header } from "./components/header/Header";
import { SearchBar } from "./components/searchbar/SearchBar";

function App() {
  const [initialData, setInitialData] = useState([]);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchTerm, updateSearchTerm] = useState(null);
  const [checkedIds, setCheckedIds] = useState([]);
  const url = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

  useEffect(() => {
    setLoading(true)
    axios.get(url)
      .then(res => {
        return res.data.slice(0, 50);
      })
      .then((res) => {
        return res.sort((a, b) => (a.last_name > b.last_name) ? 1 : -1);
      })
      .then((sortedData) => {
        setData(sortedData)
        setInitialData(sortedData)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  const handleClick = (id) => {
    const newState = checkedIds.includes(id) ?
      checkedIds.filter(searchId => searchId !== id) :
      [...checkedIds, id];

    setCheckedIds(newState);
    console.log("checked Ids collection: ", newState);
  }

  const handleSearchTermUpdate = (value) => {
    if (!value) {
      setData(initialData);
    }

    const filteredData = [...initialData].filter(contact => {
      if (!value) {
        return true;
      }
      const firstNameMatch = contact.first_name.toLowerCase().includes(value.toLowerCase());
      const lastNameMatch = contact.last_name.toLowerCase().includes(value.toLowerCase());
      return firstNameMatch || lastNameMatch
    })

    setData(filteredData);
    updateSearchTerm(value);
  };

  return (
    <Grid container>
      <Grid item sm={12}>
        <Header/>
      </Grid>

      <Grid item sm={12}>
        <SearchBar searchTerm={searchTerm} updateSearchTerm={(value) => handleSearchTermUpdate(value)}/>
      </Grid>

      <Grid item sm={12}>
        {
          loading ?
            (
              <Backdrop
                style={{ zIndex: 10 }}
                open={loading}
                onClick={() => setLoading(false)}
              >
                <CircularProgress size={100}/>
              </Backdrop>
            ) : (
              <TableContainer>
                <Table>
                  <TableBody>
                    {
                      data.map((contact, index) => {
                        return (
                          <TableRow
                            key={contact.id + contact.first_name + index} hover onClick={() => handleClick(contact.id)}
                          >
                            <TableCell>
                              <Box display="flex">
                                <Avatar>{`${contact.first_name[0]} ${contact.last_name[0]}`}</Avatar>
                                <Box ml="20px">
                                  <Typography display="block">
                                    {contact.first_name}
                                  </Typography>
                                  <Typography display="block">
                                    {contact.last_name}
                                  </Typography>
                                </Box>
                              </Box>
                            </TableCell>

                            <TableCell>
                              <Checkbox checked={checkedIds.includes(contact.id)}/>
                            </TableCell>
                          </TableRow>
                        )
                      })
                    }
                  </TableBody>
                </Table>
              </TableContainer>
            )
        }
      </Grid>
    </Grid>
  )
}

export default App;
